﻿namespace AspNet.Events.Contract.Dto
{
    public sealed class InvitedFriendDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
