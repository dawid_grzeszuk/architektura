﻿using System.Collections.Generic;

namespace AspNet.Events.Contract.Dto
{
    public sealed class EventDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IList<InvitedFriendDto> InvitedFriends { get; set; }

        public EventDto()
        {
            InvitedFriends = new List<InvitedFriendDto>();
        }
    }
}
