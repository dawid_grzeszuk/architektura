﻿using AspNet.Events.Contract.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNet.Events.Contract.Services
{
    public interface IEventService : IDependency
    {
        void AddEventType(EventTypeDto eventTypeDto);
        IList<EventDto> GetEventDtos();
        void AddEvent(EventDto eventDto);
    }
}
