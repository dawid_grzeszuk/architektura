﻿using AspNet.Events.Contract.Services;
using AspNet.Events.Dal;
using AspNet.Events.Dal.Models;
using AspNet.Events.Dal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using AspNet.Events.Contract.Dto;

namespace AspNet.Events.Service
{
    public class EventService : IEventService
    {
        private IRepository<EventType> _eventTypeRepository;
        private IRepository<Event> _eventRepository;
        private IUnitOfWork _unitOfWork;

        public EventService(IUnitOfWork unitOfWork, IRepository<EventType> eventTypeRepository, IRepository<Event> eventRepository)
        {
            _unitOfWork = unitOfWork;
            _eventTypeRepository = eventTypeRepository;
            _eventRepository = eventRepository;
        }

        public void AddEventType(AspNet.Events.Contract.Dto.EventTypeDto eventTypeDto)
        {
            _eventTypeRepository.Add(new EventType { Name = eventTypeDto.Name });
            _unitOfWork.Commit();
        }

        public IList<EventDto> GetEventDtos()
        {
            return _eventRepository.GetAll().Select(x => new EventDto
            {
                Name = x.Title,
                Description = x.Description,
                InvitedFriends = x.InvitedFriends.Select(y=> new InvitedFriendDto
                {
                    FirstName = y.FirstName,
                    LastName = y.LastName,
                    Email = y.Email

                }).ToList()

            }).ToList();
        }

        public void AddEvent(EventDto eventDto)
        {
            _eventRepository.Add(new Event
            {
                Title =  eventDto.Name,
                Description = eventDto.Description,
                Created = DateTime.Now,
                InvitedFriends = eventDto.InvitedFriends.Select(x=> new InvitedFriend
                {
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email

                }).ToList()
            });

            _unitOfWork.Commit();
        }
    }
}
