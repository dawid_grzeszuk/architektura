﻿using System.Web.Mvc;
using AspNet.Events.Contract.Dto;
using AspNet.Events.Contract.Services;

namespace AspNET.GenericRepository.Controllers
{
    public class EventController : Controller
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }

        public ActionResult Event()
        {
            return View(_eventService.GetEventDtos());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OnPostEvent(EventDto newEvent)
        {
            _eventService.AddEvent(newEvent);
            return JavaScript("window.location = '" + Url.Action("Event", "Event") + "'");
        }
    }
}
